# go-jeq

moved to [[https://bitbucket.org/mindaugas_z/go-silent-snake/src/master/libs/]]

Jobs event queue

```go
func main() {

	j := jeq.New()

	j.OnTimeout = func(id string, payload interface{}) {
		println("timeout job:", id, "with:", pl.(string))
		//mq.Publish(topic, id, payload)
	}

	payload := "payload as string"

	id := j.Add(1*time.Second, j.NewJob(payload, func(i interface{}) {
		println("job done: ", i.(string))
	}))

	// mq.publish(topic, id, payload)

	time.Sleep(2 * time.Second)

	//on mq message(id, response) { j.Done(id, response) }
	j.Emit(id, "response")
	// j.Emit(id, "response2")

	j.Remove(id)
}
```